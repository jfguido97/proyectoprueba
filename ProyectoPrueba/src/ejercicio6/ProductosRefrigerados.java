/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;

/**
 *
 * @author estudiante
 */
public class ProductosRefrigerados extends Dispensa {
    private int codOrganismo;
    private int tempMantenimiento;

    public ProductosRefrigerados(int codOrganismo, int tempMantenimiento, int fechaCaducida, int numeroLote, String fechaEnvasado, String paisOrigen) {
        super(fechaCaducida, numeroLote, fechaEnvasado, paisOrigen);
        this.codOrganismo = codOrganismo;
        this.tempMantenimiento = tempMantenimiento;
    }
     public String getAtributos() {

        return "Numero de Lote: " + numeroLote
                + "\nFecha de Envasado: " + fechaEnvasado
                + "\nPais de Origen:" + paisOrigen
                + "\nTemperatura de Mantenimiento: " + tempMantenimiento
                + "\nCodigo de Organismo: " + codOrganismo
                + "\nFecha de Caducidad: " + fechaCaducida;
    }
    
    
}
