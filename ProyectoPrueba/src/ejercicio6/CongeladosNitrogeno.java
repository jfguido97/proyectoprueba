/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;

/**
 *
 * @author estudiante
 */
public class CongeladosNitrogeno extends ProductosCongelados{
    private String infoMetodoCongelacion;
    private int tiempoExposicion;

    public CongeladosNitrogeno(String infoMetodoCongelacion, int tiempoExposicion, int tempMantenimiento, int fechaCaducida, int numeroLote, String fechaEnvasado, String paisOrigen) {
        super(tempMantenimiento, fechaCaducida, numeroLote, fechaEnvasado, paisOrigen);
        this.infoMetodoCongelacion = infoMetodoCongelacion;
        this.tiempoExposicion = tiempoExposicion;
    }
    
}
