/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio6;

/**
 *
 * @author estudiante
 */
public class ProductosFrescos extends Dispensa {

    public ProductosFrescos(int fechaCaducida, int numeroLote, String fechaEnvasado, String paisOrigen) {
        super(fechaCaducida, numeroLote, fechaEnvasado, paisOrigen);
    }
     public String getAtributos() {

        return "Numero de Lote: " + numeroLote
                + "\nFecha de Envasado: " + fechaEnvasado
                + "\nPais de Origen:" + paisOrigen
                + "\nFecha de Caducidad: " + fechaCaducida;
    }
    
}
