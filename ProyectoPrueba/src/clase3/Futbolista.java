/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3;

/**
 *
 * @author estudiante
 */
public class Futbolista extends SeleccionFutbol {
    private int dorsal;
    private String posicion;


    public Futbolista(int id, String nombre, String apellidos, int edad,int dorsal, String posicion) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.posicion = posicion;
    }
    public void jugarPartido() {
        System.out.println("El jugador esta jugando un partido");
	}

    public void entrenar() {
        System.out.println("El jugador esta entrenando");
    }

    public void concentrarse () {
            System.out.println("El jugador esta concentrandose");
        
    }
    public void viajar() {
            System.out.println("El jugador esta viajando");
        
    }
}
    

