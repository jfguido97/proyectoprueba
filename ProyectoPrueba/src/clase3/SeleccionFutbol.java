/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase3;

/**
 *
 * @author estudiante
 */
public class SeleccionFutbol {
    public int id;
    public String nombre;
    public String apellidos;
    public int edad;

    public SeleccionFutbol(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }
     public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellido:" + apellidos
                + "\nEdad: " + edad;
    }
    public void concentrarse() {
        System.out.println("El individuo se esta concentrando?");
    }
    public void viajar() {
        System.out.println("El individuo esta viajando?");
    }
}
