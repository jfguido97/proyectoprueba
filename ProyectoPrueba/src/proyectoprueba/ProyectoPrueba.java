/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprueba;

import java.util.Random;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class ProyectoPrueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        SALIR:
        while(true){
        int menu = Integer.parseInt(JOptionPane.showInputDialog("Digite : \n"
                + "1) Numero de la suerte . \n"
                + "2) Numero Perfecto .\n"
                + "3) Adivinar Numero. \n"
                + "4) Edad Par o Impar\n"
                + "5) Palabras que riman\n"
                + "6) Salir"));
        switch (menu) {
            case 1:
                int ano = Integer.parseInt(JOptionPane.showInputDialog("Digite su año de nacimiento"));
                int mes = Integer.parseInt(JOptionPane.showInputDialog("Digite su mes de nacimiento"));
                int dia = Integer.parseInt(JOptionPane.showInputDialog("Digite su dia de nacimiento"));
                System.out.println("Su numero de la suerte es : " + log.CalcularNumeroSuerte(ano + mes + dia));

                break;
            case 2:
                int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero"));
                if (log.NumPerfecto(num)) {
                    System.out.println("El numero es perfecto");
                } else {
                    System.out.println("El numero no es perfecto");
                }
                break;
            case 3:
                Random generadorAleatorios = new Random();
                int random = 1 + generadorAleatorios.nextInt(10);
                APP:
                for (int i = 0; i < 3; i++) {
                      num = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero a adivinar"));
                      switch (log.AdivinarRandom(num, random)) {
                     case 1:
                         System.out.println("El numero es menor le quedan "+(3 - i-1)+" intentos" );
                         break;
                     case 2 :
                         System.out.println("El numero es mayor le quedan "+(3 - i-1)+" intentos" );
                         break;
                     case 3 :
                         System.out.println("Ha adivinado el numero");
                         break APP;
                }
                }
                
                break;
            case 4:
                APP:
                while (true) {
                    String nombre = JOptionPane.showInputDialog("Digite el nombre de la persona : ");
                    int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad de la persona : "));
                    log.GuardarPersona(nombre, edad);
                    int pedir = Integer.parseInt(JOptionPane.showInputDialog(" ¿Desea seguir agregando mas personas?\n"
                            + "1) SI   2)NO"));
                    if (pedir == 2) {
                        while (true) {
                            System.out.println(log.ImprimirPersona());
                             pedir = Integer.parseInt(JOptionPane.showInputDialog(" ¿Desea modificar la edad de una persona?\n"
                                    + "1) SI   2)NO"));
                            if (pedir == 2) {
                                break APP;
                            }
                            nombre = JOptionPane.showInputDialog("Digite el nombre de la persona que desea modificar: ");
                            edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad de la persona que desea modificar : "));
                            log.ModificarPersona(nombre, edad);
                        }
                    }
                }
               break;
            case 5:
                String palabra1 = JOptionPane.showInputDialog("Digite una primera palabra : ");
                String palabra2 = JOptionPane.showInputDialog("Digite una segunda palabra : ");
                System.out.println(log.ComprobarRima(palabra1, palabra2));
                break;
            case 6 :
                break SALIR;
        }

    }
    }
}
