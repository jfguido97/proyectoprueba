/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprueba;

import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class Logica {

   private Persona[][] persona ;

    public Logica() {
         persona = new Persona[2][2];
    }

    /**
     * El metodo calcula el numero de la suerte al recibir por parametros la
     * suma del año, mes y dia de la persona, para luego separar los numeros y
     * realizar una suma final.
     *
     * @param calcNum suma del año, mes y dia
     * @return resultado de la suma final, es decir el numero de la suerte.
     */
    public static Integer CalcularNumeroSuerte(int calcNum) {

        int valor4 = calcNum % 10;
        calcNum = calcNum / 10;
        int valor3 = calcNum % 10;
        calcNum = calcNum / 10;
        int valor2 = calcNum % 10;
        calcNum = calcNum / 10;
        int valor1 = calcNum % 10;
        calcNum = calcNum / 10;

        return valor4 + valor3 + valor2 + valor1;
    }

    public static boolean NumPerfecto(int num) {
        int sumNumeros = 0;
        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                sumNumeros += i;
            }
        }
        if (sumNumeros == num) {
            return true;
        }
        return false;
    }

    public static int AdivinarRandom(int num, int random) {
        if (num < random) {
            return 1;
        } else if (num > random) {
            return 2;
        }
        return 3;
    }
    public void GuardarPersona(String nombre, int edad) {
        APP:
        for (int x = 0; x < persona.length; x++) {
            for (int y = 0; y < persona[x].length; y++) {
                 if (persona[x][y] == null && !ComprobarNombres(nombre) ) {
                     persona[x][y] = new Persona(nombre, edad);
                     break APP;
                 }
            }
        }
    }
    
    public  boolean ComprobarNombres(String nombre){
         for (int x = 0; x < persona.length; x++) {
            for (int y = 0; y < persona[x].length; y++) {
                  if (persona[x][y] != null&&persona[x][y].getNombre().equals(nombre)) {
                     return true;
                 }
            }
         }
        return false;
    }
    
    public String ImprimirPersona(){
        String red = "\033[31m";
        String blue = "\033[34m";
        String reset = "\u001B[0m";
        String txt = "";
        for (int x = 0; x < persona.length; x++) {
            for (int y = 0; y < persona[x].length; y++) {
                 if (persona[x][y] != null) {
                     if(ParImpar(persona[x][y].getEdad())){
                         txt+= reset+persona[x][y].getNombre()+"-"+blue+persona[x][y].getEdad()+reset+"\n";
                     }else{
                         txt+= reset+persona[x][y].getNombre()+"-"+red+persona[x][y].getEdad()+reset+"\n";
                     }
                 }
            }
        }
        return txt;
    }
    public void ModificarPersona(String nombre, int edad) {
        for (int x = 0; x < persona.length; x++) {
            for (int y = 0; y < persona[x].length; y++) {
                if (persona[x][y] != null&&persona[x][y].getNombre().equals(nombre)) {
                    persona[x][y].setEdad(edad);
                }
            }
        }
    }
 
    
    public static boolean ParImpar(int num){
         if(num%2==0){
            return true;
        }
         return false;
    }

    public static String ComprobarRima(String palabra1, String palabra2){
        //3 ultimos caracteres de las palabras
        String str1=palabra1.substring(palabra1.length()-3, palabra1.length());
        String str2=palabra2.substring(palabra2.length()-3, palabra2.length());
        //2 ultimos caracteres de las palabras
        String str3=palabra1.substring(palabra1.length()-2, palabra1.length());
        String str4=palabra1.substring(palabra1.length()-2, palabra1.length());
        
        if (str1.equals(str2)){
            return "La palabras riman.";
        }else if (str3.equals(str4)) {
            return "La palabras riman un poco.";
        }
        
        return "Las palabras no riman.";
    }
    
}
    

